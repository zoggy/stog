(*********************************************************************************)
(*                Stog                                                           *)
(*                                                                               *)
(*    Copyright (C) 2012-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** *)

open Stog.Types;;


module XR = Xtmpl.Rewrite
module Xml = Xtmpl.Xml

let get_path_sep doc =
  match Stog.Types.get_def doc.doc_defs ("",Stog.Tags.path_sep) with
    None -> "/"
  | Some (_, [ XR.D s ]) -> s.Xtmpl.Types.text
  | Some (_, xmls) ->
      failwith ("Invalid "^(Stog.Tags.path_sep^": "^(XR.to_string xmls)))
;;

let mk_doc path_sep doc_id (stog,doc) = function
  XR.D _ | XR.C _ | XR.PI _ -> (stog,doc)
| XR.E { XR.name = ("","contents") ; atts ; subs } ->
    begin
      match XR.get_att_cdata atts ("","type") with
        None ->
          let msg = "Missing type attribute in <contents> in "^
            (Stog.Path.to_string doc.doc_path)
            in
            Stog.Log.err (fun m -> m "%s" msg);
            (stog, doc)
      | Some typ ->
          let atts = XR.atts_remove ("","type") atts in
          (match doc.doc_body with
            [] -> ()
           | _ ->
               Stog.Log.warn
                 (fun m -> m "Element %s: more than one <contents> node"
                  (Stog.Path.to_string doc.doc_path)
                 )
          );
          let doc = { doc with doc_body = subs ; doc_type = typ } in
          let doc = Stog.Io.fill_doc_from_atts_and_subs doc atts subs in
          (stog, doc)
    end
| XR.E { XR.name = (_,typ) ; atts ; subs } ->
    let path =
      match XR.get_att_cdata atts ("","path") with
        Some path -> Stog.Path.of_string path
      | None ->
          match XR.get_att_cdata atts ("","id") with
            None ->
              let msg = "No id and no path attributes for an element in "^
                (Stog.Path.to_string doc.doc_path)
              in
              failwith msg
          | Some id ->
              Stog.Cut.mk_path true doc.doc_path path_sep id
    in
    let new_doc = { doc with
        doc_out = None ;
        doc_type = typ ;
        doc_path = path ;
        doc_parent = Some doc.doc_path ;
      }
    in
    let new_doc = Stog.Io.fill_doc_from_atts_and_subs new_doc atts subs in
    let new_parent = { doc with doc_children = path :: doc.doc_children } in
    let stog =
      try
        let (doc_id, _) = Stog.Types.doc_by_path stog path in
        Stog.Types.set_doc stog doc_id new_doc
      with _ -> Stog.Types.add_doc stog new_doc
    in
    (stog, new_parent)
;;

let f_multi_doc stog doc_id =
  let doc = Stog.Types.doc stog doc_id in
  let xmls =
    match doc.doc_out with
      None -> doc.doc_body
    | Some xmls -> xmls
  in
  let doc = { doc with doc_body = [] ; doc_out = None } in
  let path_sep = get_path_sep doc in
  let (stog, doc) = List.fold_left (mk_doc path_sep doc_id) (stog, doc) xmls in
  Stog.Types.set_doc stog doc_id doc
  (* remove original doc ? *)
;;

let fun_level_init =
  let f_doc doc_id stog =
    let doc = Stog.Types.doc stog doc_id in
    match doc.doc_type with
      "multi" -> f_multi_doc stog doc_id
    | _ -> stog
  in
  let f env stog docs = Stog.Types.Doc_set.fold f_doc docs stog in
  Stog.Engine.Fun_stog f
;;


let level_funs =
  [
    "init", fun_level_init ;
  ]
;;

let default_levels =
  List.fold_left
    (fun map (name, levels) -> Stog.Types.Str_map.add name levels map)
    Stog.Types.Str_map.empty
    [
      "init", [ -10 ] ;
    ]
let module_name = "multi-doc";;

let make_module ?levels () =
  let levels = Stog.Html.mk_levels module_name level_funs default_levels ?levels () in
  let module M =
  struct
    type data = unit
    let modul = {
        Stog.Engine.mod_name = module_name ;
        mod_levels = levels ;
        mod_data = () ;
       }

    type cache_data = unit
    let cache_load _stog data doc t = data
    let cache_store _stog data doc = ()
  end
  in
  (module M : Stog.Engine.Module)
;;

let f stog =
  let levels =
    try Some (Stog.Types.Str_map.find module_name stog.Stog.Types.stog_levels)
    with Not_found -> None
  in
  make_module ?levels ()
;;

let () = Stog.Engine.register_module module_name f;;