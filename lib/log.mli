(*********************************************************************************)
(*                Stog                                                           *)
(*                                                                               *)
(*    Copyright (C) 2012-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Logging *)

val src : Logs.src

val loc_tag : Xtmpl.Types.loc Logs.Tag.def
val pp_loc : Format.formatter -> Xtmpl.Types.loc -> unit

type ('a, 'b) msgf =
  (?loc:Xtmpl.Types.loc ->
   ?header:string ->
     ?tags:Logs.Tag.set ->
     ('a, Stdlib.Format.formatter, unit, 'b) Stdlib.format4 -> 'a) -> 'b

val app : ('a, unit) msgf -> unit
val err : ?fatal:int -> ('a, unit) msgf -> unit
val warn : ('a, unit) msgf -> unit
val info : ('a, unit) msgf -> unit
val debug : ('a, unit) msgf -> unit

val set_level_of_string : string -> unit