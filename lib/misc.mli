(*********************************************************************************)
(*                Stog                                                           *)
(*                                                                               *)
(*    Copyright (C) 2012-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Utilities. *)

(** [string_of_file filename] returns the content of [filename]
   in the form of one string.
@raise Stdlib.Sys_error if the file could not be opened.
*)
val string_of_file : string -> string

(** [file_of_string ~file str] creates a file named
   [filename] whose content is [str].
@raise Stdlib.Sys_error if the file could not be opened.
*)
val file_of_string : file:string -> string -> unit

(** Separate the given string according to the given list of characters.
@param keep_empty is [false] by default. If set to [true],
   the empty strings between separators are kept.
*)
val split_string : ?keep_empty:bool -> string -> char list -> string list

(** [strip_string s] removes all leading and trailing spaces from the given string.*)
val strip_string : string -> string

(** [strip_blank_lines s] works as {!strip_string}, but only strips
    full blank lines, without touching spaces or tabulations. *)
val strip_blank_lines : string -> string

(** [lowercase s] lowers the case of the given string, including accentuated characters.*)
val lowercase : string -> string

(** [list_chop n l] returns the [n] first documents of list [l] or the whole
   list if [n >= List.length l].*)
val list_chop : int -> 'h list -> 'h list

val mkdir : string -> unit

(** [is_prefix pattern s] returns true if string [s] begins with [pattern].*)
val is_prefix : string -> string -> bool

(** [list_remove_doubles ?pred l] remove doubles in the given list [l], according
   to the optional equality function [pred]. Default equality function is [(=)].*)
val list_remove_doubles : ?pred:('k -> 'k -> bool) -> 'k list -> 'k list

val md5 : string -> string

val count_char : string -> char -> int

val encode_string : string -> string

val map_opt : ('a -> 'b) -> 'a option -> 'b option

val list_concat : ?sep: 'a -> 'a list -> 'a list

val dot_to_svg : string -> string

val list_compare : ('a -> 'a -> int) -> 'a list -> 'a list -> int

(** [filename_extension filename] returns extension of [filename]
  or [""] if there is no extension. *)
val filename_extension : string -> string

val safe_mkdir : string -> unit

(** [opt_of_string s] returns [None] if the string if empty
   (length is 0) or [Some s].*)
val opt_of_string : string -> string option

(** [string_of_opt s_opt] returns the empty string if
   [s_opt = None] or [s] if [s_opt = Some s].*)
val string_of_opt : string option -> string

(** Return mdification time of the given file, or None if
  the file does not exist. *)
val file_mtime : string -> float option

(** [path_under ~parent file] returns the path to [file] from [parent].
     @raise Stdlib.Failure if [parent] is not a prefix of [file].*)
val path_under : parent: string -> string -> string

val string_of_time : float -> string

